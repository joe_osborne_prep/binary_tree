import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alumniCurie02 on 22/09/2017.
 */
public class intTree {

    private int data;
    private intTree left;
    private intTree right;

    public static List pre = new ArrayList();
    public static List in = new ArrayList();
    public static List post = new ArrayList();

        public intTree (int num){
            this.data=num;
            this.left = null;
            this.right = null;
        }

        public void addNode (int num) {
            if (num < this.data) {
                if (this.left != null) {
                    this.left.addNode(num);
                } else {
                    this.left = new intTree(num);
                }

            } else {
                if (this.right != null) {
                    this.right.addNode(num);
                } else {
                    this.right = new intTree(num);
                }
            }
        }


        public void preOrder (){
            pre.add(this.data);

            if(this.left!=null){
                this.left.preOrder();
            }

            if(this.right != null){
                this.right.preOrder();
            }
        }


        public void inOrder (){
            if(this.left!=null){
                this.left.inOrder();
            }

            in.add(this.data);

            if(this.right != null){
                this.right.inOrder();
            }
        }

        public void postOrder (){
            if(this.left!=null){
                this.left.postOrder();
            }

            if(this.right != null){
                this.right.postOrder();
            }

            post.add(this.data);
        }


    public static void main(String[] args) {

        int root = 20;
        intTree intTree = new intTree(root);


        int[]nums = {15, 200, 25, -5, 0, 100, 20, 12, 126, 1000, -150};
        for (int i : nums){
            intTree.addNode(i);
        }


        intTree.preOrder();
        System.out.println("pre list:\n"+pre);
        System.out.println("\n");

        intTree.inOrder();
        System.out.println("in list:\n"+in);
        System.out.println("\n");

        intTree.postOrder();
        System.out.println("post list:\n"+post);
        System.out.println("\n");
    }

}
