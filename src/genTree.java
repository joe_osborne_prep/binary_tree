import java.util.ArrayList;
import java.util.List;

public class genTree <T extends Comparable<T>>{


    private T data;
    private genTree left;
    private genTree right;

    public static List pre = new ArrayList();
    public static List in = new ArrayList();
    public static List post = new ArrayList();

    public genTree (T thing){
        this.data= thing;
        this.left = null;
        this.right = null;
    }

    public void addNode (T thing) {
        if (thing.compareTo(this.data)< 0) {
            if (this.left != null) {
                this.left.addNode(thing);
            } else {
                this.left = new genTree(thing);
            }

        } else {
            if (this.right != null) {
                this.right.addNode(thing);
            } else {
                this.right = new genTree(thing);
            }
        }
    }

    public void preOrder (){
        pre.add(this.data);

        if(this.left!=null){
            this.left.preOrder();
        }

        if(this.right != null){
            this.right.preOrder();
        }
    }

    public void inOrder (){
        if(this.left!=null){
            this.left.inOrder();
        }

        in.add(this.data);

        if(this.right != null){
            this.right.inOrder();
        }
    }

    public void postOrder (){
        if(this.left!=null){
            this.left.postOrder();
        }

        if(this.right != null){
            this.right.postOrder();
        }

        post.add(this.data);
    }





    public static void main(String[] args) {

        System.out.println("Type of Chars:");
        char root = 'e';
        genTree genTree = new genTree(root);

        char [] chars = {'v', 'f', 'b', 'q', 'j', 'a', 'g', 'z', 'o', 'd', 'c'};
        for (char i : chars){
            genTree.addNode(i);
        }

        genTree.preOrder();
        System.out.println("pre list:\n"+pre);
        System.out.println("\n");

        genTree.inOrder();
        System.out.println("in list:\n"+in);
        System.out.println("\n");

        genTree.postOrder();
        System.out.println("post list:\n"+post);
        System.out.println("\n");

        /////////////////////////////////////////////////////////////////////////////////////

        pre = new ArrayList();
        in = new ArrayList();
        post = new ArrayList();
        System.out.println("Type of Ints:");
        int root1 = 5;
        genTree genTree1 = new genTree(root1);

        int[]nums = {15, 200, 25, -5, 0, 100, 20, 12, 126, 1000, -150};
        for (int i : nums){
            genTree1.addNode(i);
        }

        genTree1.preOrder();
        System.out.println("pre list:\n"+pre);
        System.out.println("\n");

        genTree1.inOrder();
        System.out.println("in list:\n"+in);
        System.out.println("\n");

        genTree1.postOrder();
        System.out.println("post list:\n"+post);
        System.out.println("\n");


        ////////////////////////////////////////////////////////////////////////////////////

        pre = new ArrayList();
        in = new ArrayList();
        post = new ArrayList();
        System.out.println("Type of Doubles:");
        double root2 = 5;
        genTree genTree2 = new genTree(root2);

        double []dubs = {15.5, 20.0, 2.5, -5.0, 0.6, 10.0, 20.5, 1.2, 19.5, 10.10, -1.50};
        for (double i : dubs){
            genTree2.addNode(i);
        }

        genTree2.preOrder();
        System.out.println("pre list:\n"+pre);
        System.out.println("\n");

        genTree2.inOrder();
        System.out.println("in list:\n"+in);
        System.out.println("\n");

        genTree2.postOrder();
        System.out.println("post list:\n"+post);
        System.out.println("\n");




    }


}